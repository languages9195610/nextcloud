# Backup your files into nextcloud

- [API docs](https://docs.nextcloud.com/server/latest/developer_manual/client_apis/WebDAV/basic.html)

How to start
- Create an auth data in web-ui and put it into .env
- Let's run the script! 

There are two scripts:
- backup files from gitlab
- delete files older than X days
