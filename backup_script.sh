#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# set env variables
# shellcheck disable=SC1091
source .env

mkdir -p gitlab-backups
# https://docs.gitlab.com/ee/administration/backup_restore/backup_gitlab.html?tab=Linux+package+%28Omnibus%29#excluding-specific-directories-from-the-backup
ssh "$SSH_STR" "sudo gitlab-rake gitlab:cleanup:orphan_job_artifact_files DRY_RUN=false \
	       && sudo -u git gitlab-backup create SKIP=artifacts"

# shellcheck disable=SC2029
BACKUP_NAME="$(ssh goto@gitlab.csp "sudo ls -1tr /var/opt/gitlab/backups/ | grep $(date +%Y_%m_%d) | tail -1")"

# shellcheck disable=SC2029
ssh "$SSH_STR" "mkdir -pv /tmp/gitlab-backups/ \
	       && sudo cp /var/opt/gitlab/backups/$BACKUP_NAME /tmp/gitlab-backups/ \
	       && sudo tar -zcf /tmp/gitlab-backups/etc-gitlab-$BACKUP_NAME /etc/gitlab \
	       && sudo chown -v goto:goto /tmp/gitlab-backups/$BACKUP_NAME"

scp "$SSH_STR:/tmp/gitlab-backups/$BACKUP_NAME" "$PATH_TO_BACKUP/$BACKUP_NAME"
scp "$SSH_STR:/tmp/gitlab-backups/etc-gitlab-$BACKUP_NAME" "$PATH_TO_BACKUP/etc-gitlab-$BACKUP_NAME"

curl -k -u "$USER:$PASS" -T "$PATH_TO_BACKUP/$BACKUP_NAME" "$NEXT_CLOUD_URL/remote.php/webdav/$NEXTCLOUD_FOLDER/$BACKUP_NAME"
curl -k -u "$USER:$PASS" -T "$PATH_TO_BACKUP/etc-gitlab-$BACKUP_NAME" "$NEXT_CLOUD_URL/remote.php/webdav/$NEXTCLOUD_FOLDER/etc-gitlab-$BACKUP_NAME"

# shellcheck disable=SC2029
ssh "$SSH_STR" "rm -v /tmp/gitlab-backups/$BACKUP_NAME && rm -v /tmp/gitlab-backups/etc-gitlab-$BACKUP_NAME"
