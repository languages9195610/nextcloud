#!/usr/bin/env bash
 
set -o errexit
set -o nounset
set -o pipefail
 
# set env variables
# shellcheck disable=SC1091
source .env

# Delete file
function delete() {
# local FILE_TO_DELETE
  local FILE_TO_DELETE=$1
  if [[ -n "$FILE_TO_DELETE" ]]; then
    curl "$NEXT_CLOUD_URL/$FILE_TO_DELETE" \
      --silent \
      --show-error \
      --user "$USER:$PASS" \
      --request DELETE
    return 0
  else
    printf "Error: %s, line: %s. Variable doesn't set.\n" "$?" "$LINENO"
    exit 198
  fi
}

# Get files from $NEXTCLOUD_FOLDER
# LIST_OF_FILES -t array < <(mycommand)
LIST_OF_FILES=($(curl "$NEXT_CLOUD_URL/remote.php/webdav/$NEXTCLOUD_FOLDER" \
  --silent \
  --show-error \
  --user "$USER:$PASS" \
  --request PROPFIND \
  --data @data1.xml \
  | /usr/local/bin/xq | grep '<d:href>' | grep ".tar" | cut -d '>' -f2 | cut -d '<' -f1 ))

# Make a list of files with expired lifetime
DAYS=$((60*60*24))
for file in "${LIST_OF_FILES[@]}"; do
  # Get date of backup
  BACKUP_DATE=$(echo "$file" | sed -r 's/_/-/g' | grep -o "20[1-9][1-9]-[0-9][0-9]-[0-9][0-9]";)
  DATE_DIFF=$(( (`date +%s` - `date +%s -d "$BACKUP_DATE"`) / $DAYS ))
  echo $DATE_DIFF

  # Comparing and deleting
  if [[ $DATE_DIFF -gt $DELETE_AFTER_DAYS ]]; then
    echo $file
    delete $file
  fi
done

exit 0